import * as React from 'react';
import styled from '@emotion/styled';
import Link from 'next/link';

import { Text, Box, themeProps, UnstyledAnchor } from 'components/design-system';
import { logEventClick } from 'utils/analytics';

import Content from './Content';
import Column from './Column';

import FacebookIcon from '../../assets/Icons/FacebookIcon';
import InstagramIcon from '../../assets/Icons/InstagramIcon';
import TwitterIcon from '../../assets/Icons/TwitterIcon';

const FooterElement = Content.withComponent('footer');

const Root = styled(FooterElement)`
  padding: 48px 24px 84px;
`;

const TextLink = styled(Text)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const FooterGrid = styled(Box)`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-areas:
    'socials'
    'copyright'
    'privacy';
  grid-gap: 16px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: 1fr 1fr 1fr;
    grid-template-areas: 'copyright socials privacy';
  }
`;

const FooterCopyright = styled(Box)`
  grid-area: copyright;
  justify-content: center;

  ${themeProps.mediaQueries.md} {
    justify-content: flex-start;
  }
`;

const FooterSocials = styled(Box)`
  grid-area: socials;
`;

const SocialLink = styled(UnstyledAnchor)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
`;

const FooterPrivacy = styled(Box)`
  grid-area: privacy;
  justify-content: center;

  ${themeProps.mediaQueries.md} {
    justify-content: flex-end;
  }
`;

const Footer: React.FC = () => {
  return (
    <Root noPadding noFlex>
      <Column>
        <Box
          display="flex"
          flexDirection="column"
          spacing="sm"
          paddingTop="md"
          borderTopWidth="1px"
          borderTopStyle="solid"
          borderTopColor="accents01"
        >
          <FooterGrid>
            <FooterCopyright display="flex" alignItems="center">
              <Text variant={300} as="p" color="accents07">
                Hak Cipta &copy; 2020 &middot; kawalcovid19.id
              </Text>
            </FooterCopyright>
            <FooterSocials display="flex" justifyContent="center">
              <Box display="grid" gridTemplateColumns="24px 24px 24px" gridGap="md">
                <SocialLink
                  href="https://instagram.com/kawalcovid19.id"
                  target="_blank"
                  rel="noopener noreferrer"
                  onClick={() => logEventClick('Instagram')}
                >
                  <InstagramIcon fill="#f1f2f3" height={24} />
                </SocialLink>
                <SocialLink
                  href="https://twitter.com/KawalCOVID19"
                  target="_blank"
                  rel="noopener noreferrer"
                  onClick={() => logEventClick('Twitter')}
                >
                  <TwitterIcon fill="#f1f2f3" height={20} />
                </SocialLink>
                <SocialLink
                  href="https://www.facebook.com/KawalCOVID19"
                  target="_blank"
                  rel="noopener noreferrer"
                  onClick={() => logEventClick('Facebook')}
                >
                  <FacebookIcon fill="#f1f2f3" height={24} />
                </SocialLink>
              </Box>
            </FooterSocials>
            <FooterPrivacy display="flex" alignItems="center">
              <Link href="/kebijakan-privasi" passHref>
                <TextLink
                  variant={300}
                  as="a"
                  display="inline-block"
                  fontWeight={700}
                  color="accents07"
                >
                  Kebijakan Privasi
                </TextLink>
              </Link>
            </FooterPrivacy>
          </FooterGrid>
        </Box>
      </Column>
    </Root>
  );
};

export default Footer;
